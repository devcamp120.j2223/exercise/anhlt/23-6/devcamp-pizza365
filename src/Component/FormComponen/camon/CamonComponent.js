import { Component } from "react";
class CamonKachhangComponent extends Component {
    render() {
        return (
            <div>
                <div class="text-center p-4 mt-4 text-danger">
                    <h2><b class="border-bottom">Gửi đơn hàng</b></h2>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="fullname">Họ tên</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="inp-fullname"
                                    placeholder="nhập tên"
                                />
                                <div class="form-group">
                                    <label for="dien-thoai">Email</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="inp-email"
                                        placeholder="nhập số điện thoại"
                                    />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dien-thoai">Số điện thoại</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="inp-phone-number"
                                    placeholder="nhập số điện thoại"
                                />
                            </div>
                            <div class="form-group">
                                <label for="dia-chi">địa chỉ</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="inp-address"
                                    placeholder="nhập địa chỉ"
                                />
                            </div>
                            <div class="form-group">
                                <label for="message">Mã giảm giá</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="inp-vocuher"
                                    placeholder="Mã Giảm giá"
                                />
                            </div>
                            <div class="form-group">
                                <label for="message">Lời nhắn</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="inp-loinhan"
                                    placeholder="lời nhắn"
                                />
                            </div>
                            <button type="button" class="btn btn-primary w-100 font-weight-bold" id="btn-gui-don">Gủi</button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
export default CamonKachhangComponent;