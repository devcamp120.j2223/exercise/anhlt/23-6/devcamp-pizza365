import { Component } from "react";
import NavanHeader from "./Header/Nav";
import Carouselcomponent from "./carousel/carouselComponent";
import TittleComponent from "./tittle/tittleComponent";
class HeaderComponent extends Component {
    render(){
        return(
            <div>
               
                <NavanHeader/>
                < TittleComponent />
                < Carouselcomponent />
                {/* ----hết header---- */}

            </div>
        )
    }
}
export default HeaderComponent;