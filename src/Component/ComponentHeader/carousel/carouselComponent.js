import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import anh1 from "../../../assets/images/1.jpg"
import anh2 from "../../../assets/images/2.jpg"
import anh3 from "../../../assets/images/3.jpg"
import anh4 from "../../../assets/images/4.jpg"

class Carouselcomponent extends Component {
    render(){
        return(
            <div className='container'>
            <div className="col-sm-12">
              <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img className="d-block w-100" src={anh1} alt="First slide" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" src={anh2} alt="Second slide" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" src={anh3} alt="Third slide" />
                  </div>
                  <div className="carousel-item">
                    <img className="d-block w-100" src={anh4} alt="four slide" />
                  </div>
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span className="carousel-control-next-icon" aria-hidden="true"></span>
                  <span className="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        )
    }
}
export default Carouselcomponent;