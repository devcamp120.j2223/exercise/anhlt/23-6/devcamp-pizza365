import { Component } from "react";
import seafood from '../../../assets/images/seafood.jpg'
import hawaiian from '../../../assets/images/hawaiian.jpg'
import bacon  from '../../../assets/images/bacon.jpg'
class LoaiPizzaComponent extends Component {
    render() {
        return (
            <div>
                <div class="text-center p-4 mt-4 text-danger">
                    <h2><b class="border-bottom">Chọn Loại Pizza</b></h2>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                       
                        <div class="col-sm-4">
                            <div className="card w-100" style={{ width: '18rem' }}>
                                <img src={seafood} class="card-img-top" alt='seafood' />
                                <div className="card-body text-center">
                                    <h3 class="text-danger"><b>OCEAN MANIA</b></h3>
                                    <p class="text-info">PIZZA HẢI SẢN SỐT MAYONNAISE</p>
                                    <p>Xốt cà chua, phô mai mozzarealla, tôm, mực, thanh cua, hành tây.</p>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-warning form-control" id="pizza-ocean-mania">Chọn</button>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-sm-4">
                            <div class="card w-100" style={{ width: '18rem' }}>
                                <img src={hawaiian} class="card-img-top" alt='hawaiian' />
                                <div class="card-body text-center">
                                    <h3 class="text-danger"><b>HAWAIIAN</b></h3>
                                    <p class="text-info">PIZZA DĂM BÔNG DỨA KIỂU HAWAI</p>
                                    <p>Xốt cà chua, phô mai mozzarealla, tôm, mực, thị dăm bông, thơm.</p>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-warning form-control" id="pizza-hawai">Chọn</button>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-sm-4">
                            <div class="card w-100" style={{ width: '18rem' }}>
                                <img src={bacon} class="card-img-top" alt="bacon" />
                                <div class="card-body text-center">
                                    <h3 class="text-danger"><b>BACON</b></h3>
                                    <p class="text-info">PIZZA PHÔ MAI THỊT HEO XÔNG KHÓI </p>
                                    <p>Xốt phô mai mozzarealla, thịt Heo muối, thịt gà, thịt dăm bông, cà chua</p>
                                </div>
                                <div class="card-footer text-center">
                                    <button class="btn btn-warning form-control" id="pizza-thit-hun-khoi">Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default LoaiPizzaComponent;