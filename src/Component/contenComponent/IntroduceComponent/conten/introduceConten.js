import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
class ContenIntroduce extends Component {
    render() {
        return (
            <div className="col-sm-12">
                <div className="row">
                    <div className="col-sm-3 p-4 bg-info text-danger">
                        <h2 className="text-center">Đa dạng</h2>
                        <p className="text-center ">số lượng Pizza đa dạng, có đầy đủ các loại Pizza đang hot nhất hiện nay</p>
                    </div>
                    <div className="col-sm-3 p-4 bg-primary text-warning text-center">
                        <h2>Chất lượng</h2>
                        <p>nguyên liệu sạch sẽ 100% rõ nguồn gốc, quy trình chế biến bảo đãm vệ sinh an toàn thực phẩm</p>
                    </div>
                    <div className="col-sm-3 p-4 bg-success text-center text-danger">
                        <h2>Hương vị</h2>
                        <p>Đảm bảo hương vị ngon độc,lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365</p>
                    </div>
                    <div className="col-sm-3 p-4 bg-warning text-center text-dark">
                        <h2>dịch vụ</h2>
                        <p>Nhân viên thân thiện,nhà hàng hiện đại, dịch vụ giao hàng nhanh chất lượng, tân tiến</p>
                    </div>
                </div>
            </div>
        )
    }
}
export default ContenIntroduce;