import { Component } from "react";
class ContenSizeComponent extends Component{
    render (){
        return(
            <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-4">
                <div class="card">
                  <div class="card-header bg-warning text-center">
                    <h3>S (small)</h3>
                  </div>
                  <div class="card-body text-center">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><b>Đường kính: </b> 20cm</li>
                      <li class="list-group-item"><b>Sường nướng: </b> 2</li>
                      <li class="list-group-item"><b>Salad:</b> 200g</li>
                      <li class="list-group-item"><b>Nước ngọt: </b> 2</li>
                      <li class="list-group-item">
                        <h1><b>150.000</b></h1>
                        <p><b>VND</b></p>
                      </li>
                    </ul>
                  </div>
                  <div class="card-footer bg-warning text-center">
                    <button class="form-control btn btn-primary" id="btn-S-chon">Chọn</button>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="card">
                  <div class="card-header bg-warning text-center">
                    <h3>M (Medium)</h3>
                  </div>
                  <div class="card-body text-center">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><b>Đường kính:</b> 25cm</li>
                      <li class="list-group-item"><b>Sường nướng: </b> 4</li>
                      <li class="list-group-item"><b>Salad:</b> 300g</li>
                      <li class="list-group-item"><b>Nước ngọt: </b> 3</li>
                      <li class="list-group-item">
                        <h1><b>200.000</b></h1>
                        <p><b>VND</b></p>
                      </li>
                    </ul>
                  </div>
                  <div class="card-footer bg-warning text-center">
                    <button class="form-control btn btn-primary" id="btn-M-chon">Chọn</button>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="card">
                  <div class="card-header bg-warning text-center">
                    <h3>L (Large)</h3>
                  </div>
                  <div class="card-body text-center">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item"><b>Đường kính:</b> 30cm</li>
                      <li class="list-group-item"><b>Sường nướng: </b> 8</li>
                      <li class="list-group-item"><b>Salad:</b> 500g</li>
                      <li class="list-group-item"><b>Nước ngọt: </b> 4</li>
                      <li class="list-group-item">
                        <h1><b>250.000</b></h1>
                        <p><b>VND</b></p>
                      </li>
                    </ul>
                  </div>
                  <div class="card-footer bg-warning text-center">
                    <button class="form-control btn btn-primary" id="btn-L-chon">Chọn</button>
                  </div>
                </div>
              </div>
    
            </div>
          </div>
        )
    }
}
export default ContenSizeComponent;
