import { Component } from "react";
import ContenIntroduceComponent from "./IntroduceComponent/IntroduceContenComponent";
import ContenSizePizzaComponent from "./Sizecomponent/ContenSizeCompoent";
import LoaiPizzaComponent from "./LoaiPizza/LoaiPizaa";
class ContenComponent extends Component {
    render(){
        return (
          <div>
            < ContenIntroduceComponent />
            < ContenSizePizzaComponent/>
            < LoaiPizzaComponent/>
          </div>
        )
    }
}
export default ContenComponent;