import { Component } from "react";
class DrinkPizzaComponent extends Component {
    render() {
        return (
            <div>
                <div class="text-center p-4 mt-4 text-warning">
                    <h2><b class="border-bottom">Chọn đồ uông</b></h2>
                </div>
                <div class="col-sm-12">
                    <select id="select-do-uong" class=" form-control">
                        <option value="0">chọn loại dồ uống</option>
                    </select>
                </div>
            </div>
        )
    }
}
export default DrinkPizzaComponent;