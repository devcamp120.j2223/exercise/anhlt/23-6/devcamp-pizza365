import { Component } from "react";
class FooterPizzaComponent extends Component {
    render() {
        return (
            <div>
                <div class="container-fluid bg-warning p-4 mt-4">
                    <div class="row text-center">
                        <div class="col-sm-12">
                            <h4 class="m-2">Footer</h4>
                            <a href="#" class="btn btn-dark m-3"><i class="fa fa-arrow-up"></i>To the top</a>
                            <div class="m-2">
                                <i class="fa fa-facebook-official w3-hover-opacity"></i>
                                <i class="fa fa-instagram w3-hover-opacity"></i>
                                <i class="fa fa-snapchat w3-hover-opacity"></i>
                                <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                                <i class="fa fa-twitter w3-hover-opacity"></i>
                                <i class="fa fa-linkedin w3-hover-opacity"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default FooterPizzaComponent;