
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import HeaderComponent from './Component/ComponentHeader/ComponentHeader';
import ContenComponent from './Component/contenComponent/ContenComponet';
import DrinkPizzaComponent from './Component/DrinkComponent/DrinkComponent';
import FromPizzaComponent from './Component/FormComponen/FromPizzaComponent';
import FooterPizzaComponent from './Component/FooterComponent/footerComponent';

function App() {
  return (
    <div className="App">
      <div>
      {/* header */}
        < HeaderComponent />
      </div>
      {/* conten*/}
      < ContenComponent />
      {/* -----Drink--- */}
      < DrinkPizzaComponent />
      {/* ------gui don hang------- */}
      < FromPizzaComponent />
      {/* <!-- Footer --> */}
      < FooterPizzaComponent />
      {/* het footer */}

    </div>
  );
}

export default App;
